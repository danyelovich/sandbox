// cloudviewer.cpp : Defines the entry point for the console application.
//

#define IDMLOG_FATAL printf
#define IDMLOG_ERROR printf
#define IDMLOG_WARNING	printf
#define IDMLOG_INFO printf
#define IDMLOG_TRACE printf





//#include "stdafx.h"
#include <GL/glew.h>
#include "IDMGraphicsTest.h"
#include "IDMTinyMS.h"
#include "Renderer.h"
#include "RabbitListener.h"

//#include <IDMLogging.h>
//#include "IDMTinyMS.h"

#ifdef _WIN32
#pragma message("_WIN32 defined")
#elif defined (WIN32)
#pragma message("win32 defined")
#elif defined(UNIX)
#pragma message("UNIX defined")
#elif defined(__linux__)
#pragma message("__linux__ defined")
#elif defined(__MINGW32__)
#pragma message("__MINGW32__ defined")
#elif defined(__CYGWIN__)
#pragma message("__CYGWIN__ defined")
#endif



int main(int argc, char* argv[])
{
	//IDMGraphicsTest test;
	//for (int i = 0; i < 1; i++) {
	//	IDMTinyMS test("assets/MaskFeatAnimAll.idm", IDMTinyMS::TGA);
	//	std::cout << "-------------------" << std::endl;
	//	std::cout << "loading time: " << test.GetLoadingTime() * 1000 << std::endl;
	//	std::cout << "rendering time: " << test.GetRenderingTime() * 1000 << std::endl;
	//	std::cout << "total frames: " << test.GetTotalFrames() << std::endl;
	//	std::cout << "fps: " << 1.0 / (test.GetRenderingTime() / test.GetTotalFrames()) << std::endl;
	//	std::cout << "-------------------" << std::endl;
	//};
	//return 0;

	//if (!INITIALIZE_IDM_LOGGING(idm_log::IDMLogLevel_TRACE, "stdout")){
	//	bool ok = true;
	//}

	// start command processor (rabbit, cmdline, etc)
	// start renderer
	//IDMTinyMS test("assets/tests_Main.idm", IDMTinyMS::TGA);	
	idm::cv::Renderer renderer;

	idm::cv::RabbitListener listener;
	if (!listener.Init(&renderer, "localhost", 5672, "cv_cmd_queue", "cv_done_queue", "guest", "guest", 4))
	{
		IDMLOG_FATAL("[FATAL]: Could not initialize rabbit listener.\n");
		//renderer.Stop();
		return 1;
	}

	if (!renderer.Start()){
		IDMLOG_FATAL("[FATAL]: Could not start renderer.\n");
		listener.CleanUp();
		return 1;
	}

	if (!listener.Start()){
		IDMLOG_FATAL("[FATAL]: Could not start rabbit listener.\n");
		renderer.Stop();
		listener.CleanUp();
		return 1;
	}

	// wait for listener's condition variable
	listener.WaitUntilDone();
	listener.Stop();
	renderer.Stop();
	listener.CleanUp();
	



	//// do nothing for 3 seconds
	//std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	//if (false){
	//	// laod MaskFeatAnimAll.idm
	//	std::string idmPath = std::string("assets/MaskFeatAnimAll.idm");
	//	printf("Pushing command to load idm %s.\n", idmPath.c_str());
	//	idm::cv::CmdLoadIdm* loadCmd = new idm::cv::CmdLoadIdm(&renderer, idmPath);
	//	renderer.SubmitCommand(loadCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	//	idm::cv::CmdSetAsset* setCmd = new idm::cv::CmdSetAsset(&renderer, 21, "assets/019.jpg");
	//	renderer.SubmitCommand(setCmd);
	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 22, "assets/019.jpg");
	//	renderer.SubmitCommand(setCmd);
	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 23, "assets/019.jpg");
	//	renderer.SubmitCommand(setCmd);
	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 24, "assets/019.jpg");
	//	renderer.SubmitCommand(setCmd);
	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 25, "assets/019.jpg");
	//	renderer.SubmitCommand(setCmd);

	//	std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	//}
	//else
	//{
	//	// text bbox
	//	std::string idmPath = std::string("assets/TextBBox.idm");
	//	printf("Pushing command to load idm %s.\n", idmPath.c_str());
	//	idm::cv::CmdLoadIdm* loadCmd = new idm::cv::CmdLoadIdm(&renderer, idmPath);
	//	renderer.SubmitCommand(loadCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	//	idm::cv::CmdSetAsset* setCmd = new idm::cv::CmdSetAsset(&renderer, 7, "Hello hi \n what on earth are you talking about?");
	//	renderer.SubmitCommand(setCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 7, "Hello hi \n what on earth are you talking about\nmother\nfucker?");
	//	renderer.SubmitCommand(setCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 7, "Hello hi \n what on earth are you talking about\nmother\nfucker\nawefawef?");
	//	renderer.SubmitCommand(setCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 7, "Hello hi \n what on earth are you talking about\nmother\nfucker\nawefawef\nsiotjt?");
	//	renderer.SubmitCommand(setCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 7, "Hello hi \n what on earth are you talking about\nmother\nfucker\nawefawef\nsiotjt\nauierfoj?");
	//	renderer.SubmitCommand(setCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	//}

	//

	//renderer.Stop();
	
	
	return 0;
}

