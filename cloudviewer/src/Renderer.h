#pragma once
#ifndef IDM_CV_RENDERER
#define IDM_CV_RENDERER

#include <gl/GLView.h>
#include <GLFW/glfw3.h>
#include <utils/Timer.h>
#include <resources/idm/IDMLoader.h>
#include <concurrency/dataStructures/ThreadSafeQueue.h>
#include <globals/IDMGlobalDefs.h>
#include <atomic>
#include <thread>
#include <mutex>

namespace idm
{
	class IDMLoader;
	class IDMExporter;
	class IDMImage;

	namespace cv
	{
		class Renderer;

		enum RenderCmd : uint32_t {
			eRCUnknown = 0,
			eRCSetAsset,
			eRCSetFps,
			eRCSetState,
			eRCSetFrameId,
			eRCLoadIdm,
			eRCnumberof
		};

		typedef struct tagRenderCmdString {
			RenderCmd cmd;
			const char*  str;
		} RenderCmdString;

		static RenderCmdString RenderCmdStrMap[] =
		{
			{ eRCUnknown, "Unknown" },
			{ eRCSetAsset, "SetAsset" },
			{ eRCSetFps, "SetFps" },
			{ eRCSetState, "SetState" },
			{ eRCSetFrameId, "SetFrameId" },
			{ eRCLoadIdm, "LoadIdm" }
		};

		enum PlaybackState : uint32_t {
			ePSUnknown = 0,
			ePSPlay,
			ePSPlayReverse,
			ePSPaused,
			ePSnumberof
		};

		typedef struct tagPlaybackStateString {
			PlaybackState state;
			const char*  str;
		} PlaybackStateString;

		static PlaybackStateString PlaybackStateStrMap[] =
		{
			{ ePSUnknown, "Unknown" },
			{ ePSPlay, "Play" },
			{ ePSPlayReverse, "PlayReverse" },
			{ ePSPaused, "Paused" }
		};

		struct Command {
			Command(Renderer* renderer, RenderCmd cmdType) 
				: m_renderer(renderer), m_cmdType(cmdType) { }
			virtual ~Command() {}
			Renderer* m_renderer;
			RenderCmd m_cmdType;
			virtual bool operator()(void) = 0;
		};

		struct CmdSetAsset : public Command {
			CmdSetAsset(Renderer* renderer, int id, const std::string& assetInfo) 
				: Command(renderer, eRCSetAsset), m_id(id), m_assetInfo(assetInfo) {}
			~CmdSetAsset(){}
			int m_id;
			std::string m_assetInfo;
			bool operator()(void);
		};

		struct CmdSetFps : public Command {
			CmdSetFps(Renderer* renderer, float fps) 
				: Command(renderer, eRCSetFps), m_fps(fps) {}
			~CmdSetFps() {}
			float m_fps;
			bool operator()(void);
		};

		struct CmdSetState : public Command {
			CmdSetState(Renderer* renderer, PlaybackState state) 
				: Command(renderer, eRCSetState), m_state(state) {}
			~CmdSetState() {}
			PlaybackState m_state;
			bool operator()(void);
		};

		struct CmdSetFrameId : public Command {
			CmdSetFrameId(Renderer* renderer, int frameId) 
				: Command(renderer, eRCSetFrameId), m_frameId(frameId) {}
			~CmdSetFrameId() {}
			int m_frameId;
			bool operator()(void);
		};

		struct CmdLoadIdm : public Command {
			CmdLoadIdm(Renderer* renderer, const std::string& idmPath) 
				: Command(renderer, eRCLoadIdm), m_idmPath(idmPath) {}
			~CmdLoadIdm() {}
			std::string m_idmPath;
			bool operator()(void);
		};

		struct DoneRequest {
			uint64_t id;
			uint32_t errorCode;
		};


		class Renderer
		{
			IDM_DISABLE_COPY(Renderer);

		public:
			struct IdmDesc{
				IdmDesc()
					: m_width(-1)
					, m_height(-1)
					, m_frameCount(-1)
				{}

				IdmDesc(int width, int height, int framecount)
					: m_width(width)
					, m_height(height)
					, m_frameCount(framecount)
				{}

				int			m_width;
				int			m_height;
				int			m_frameCount;

			};

			/* @brief constructor
			*
			*/
			Renderer();

			/* @brief 
			*
			*/
			~Renderer();

			/* @brief Starts the render thread.
			*
			*/
			bool Start();

			/* @brief Stops the render thread
			*
			*/
			void Stop();

			/* @brief Initializes render-related objects.
			*	Should only be called from the thread where OpenGL will be used
			*	and will return false if not.
			*/
			bool InitRender();

			/* @brief Uninitializes render-related objects.
			*	Should only be called from the thread where OpenGL was used, 
			*	and will return prematurely if not.
			*/
			void CleanUpRender();

			/* @brief
			*
			*/
			bool LoadIdm(const char* idmFile);

			/* @brief
			*
			*/
			void UnloadIdm();

			/* @brief
			*
			*/
			int UpdateFrameId();

			/* @brief
			*
			*/
			int IncrementFrameId();

			/* @brief
			*
			*/
			int DecrementFrameId();

			/* @brief
			*
			*/
			void SetFrameId(int frameId);

			/* @brief
			*
			*/
			int GetFrameId() const;

			/* @brief
			*
			*/
			void SetState(PlaybackState state);
			
			/* @brief
			*
			*/
			PlaybackState GetState() const;

			/* @brief
			*
			*/
			void SetFps(float fps);

			/* @brief
			*
			*/
			float GetFps() const;

			/* @brief
			*
			*/
			bool SetAsset(uint32_t id, const char* assetRef);

			/* @brief
			*
			*/
			bool IsSprite(idm::IDMLoader::IDMTag tag);

			/* @brief
			*
			*/
			bool SubmitCommand(Command* command);

			/* @brief
			 *	
			 */
			bool GetNextDoneRequest(DoneRequest& nextDone);

		private:

			/* @brief
			*
			*/
			void UpdateExporter();

			/* @brief
			*
			*/
			void Worker();

			// the opengl context
			GLView*				m_view;

			// whether opengl is initialized
			bool				m_init;

			// object that loads idms
			IDMLoader*			m_loader;

			//  contains information about the current idm
			IdmDesc				m_desc;

			// object that performs the exporting
			IDMExporter*		m_exporter;

			// worker thread
			std::thread			m_renderThread;

			// true indicates that the worker thread should start.
			std::atomic<bool>	m_ready;

			// true indicates that the worker thread should end
			std::atomic<bool>	m_done;

			// current frame to render
			int					m_frameId;

			// playback state
			PlaybackState		m_state;

			// frame timer (used to do the throttling)
			Timer				m_timer;

			// actual timer (measures frame-to-frame time)
			Timer				m_timerActual;

			// fps
			float				m_fps;

			// for synchronization
			mutable std::mutex	m_mutex;

			// command queue
			IDMConcurrency::ThreadSafeQueue<Command*> m_cmdQueue;
			IDMConcurrency::ThreadSafeQueue<DoneRequest> m_doneQueue;

			// bookkeeping for deletion
			std::vector<IDMImage*>		m_images;
			std::vector<std::wstring*>	m_texts;
		};

	}
}


#endif