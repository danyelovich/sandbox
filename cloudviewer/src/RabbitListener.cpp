#include "RabbitListener.h"
#include "Renderer.h"
#include "CvError.h"
#include <utils/Msg.h>
#include <utils/StringUtils.h>
#include <concurrency/ThreadUtil.h>
#include <amqp_tcp_socket.h>
#include <iostream>

#ifdef _WIN32
#include <Windows.h>
#include "winsock.h"
#endif

#define IDM_LISTENER_THREAD_CONSUME_TIMEOUT_SEC 3
#define IDM_LISTENER_THREAD_CONSUME_TIMEOUT_US  1000

using namespace idm::cv;
using namespace std;

//-----------------------------------------------------------------------------
//	Extensions to rabbitmq api

//	amqp_bytes_zero: Only zeros the structure. Does NOT clear memory.
void amqp_bytes_zero(amqp_bytes_t* bytes)
{
	if (nullptr == bytes)
	{
		return;
	}
	bytes->bytes = nullptr;
	bytes->len = 0;
}

//-----------------------------------------------------------------------------
RabbitListener::RabbitListener() 
	: m_init(false)
	, m_conn_state(nullptr)
	, m_socket(nullptr)
	, m_done(true)
	, m_quitRequested(false)
	, m_renderer(nullptr)
{
	m_channel_id = 1;
	m_consumer_tag = amqp_cstring_bytes("ip_worker_01");	// will be built dynamically <ip_address>_worker_<nn>
	amqp_bytes_zero(&m_render_queue_name);
	amqp_bytes_zero(&m_done_queue_name);
	amqp_bytes_zero(&m_done_queue_default_name);
}


//-----------------------------------------------------------------------------
RabbitListener::~RabbitListener()
{
}

//---------------------------------------------------------------------------------------------
bool RabbitListener::Init(Renderer* renderer, const char* hostname, int port,
	const char* renderQueueName, const char* doneQueueDefaultName, const char* userName, const char* passWord,
	int prefetchCount)
{
	if (m_init){
		return true;
	}

	if (nullptr == renderer)
	{
		return false;
	}

	// set member variables
	m_renderer = renderer;

	if (0 != m_render_queue_name.len)
	{
		amqp_bytes_free(m_render_queue_name);
		amqp_bytes_zero(&m_render_queue_name);
	}
	m_render_queue_name = amqp_bytes_malloc_dup(amqp_cstring_bytes(renderQueueName));

	if (0 != m_done_queue_default_name.len)
	{
		amqp_bytes_free(m_done_queue_default_name);
		amqp_bytes_zero(&m_done_queue_default_name);
	}
	m_done_queue_default_name = amqp_bytes_malloc_dup(amqp_cstring_bytes(doneQueueDefaultName));

	cout << "[RabbitMQ]: 	Running on RabbitMQ version " << "\t" << amqp_version() << endl;
	cout << "[RabbitMQ]: 	Initializing communication at " << "\t" << hostname << ":" << port << endl;
	cout << "[RabbitMQ]: 	Render queue name set to: " << "\t" << renderQueueName << endl;
	cout << "[RabbitMQ]: 	Done queue name set to: " << "\t" << doneQueueDefaultName << endl;


	// build a new amqp connection state structure
	cout << "[RabbitMQ]: 	Creating connection... ";
	m_conn_state = amqp_new_connection();
	cout << "\t\tsuccess." << endl;

	// create the socket
	cout << "[RabbitMQ]:	Creating new socket... ";
	m_socket = amqp_tcp_socket_new(m_conn_state);
	if (!m_socket)
	{
		//report_error("Error creating TCP socket. Is RabbitMQ Server installed and running?");
		IDM_PRINT_ERROR(CvNetworkConnectionError, "Error creating TCP socket. Is RabbitMQ Server installed and running?");
		return false;
	}
	cout << "\t\tsuccess" << endl;

	// open the socket
	cout << "[RabbitMQ]: 	Opening socket... ";
	int status = amqp_socket_open(m_socket, hostname, port);
	if (status)
	{
		//report_error("Error opening TCP socket. Is RabbitMQ Server installed and running?");
		IDM_PRINT_ERROR(CvNetworkConnectionError, "Error opening TCP socket. Is RabbitMQ Server installed and running?");
		return false;
	}
	cout << " \t\tsuccess" << endl;

	// log in to the server (TODO: guest/guest will be replaced by something idomoo specific)
	cout << "[RabbitMQ]: 	Logging in... ";
	if (!return_on_amqp_error(amqp_login(m_conn_state, "/", 0, 131072, 0, AMQP_SASL_METHOD_PLAIN, userName, passWord), "Logging in"))
	{
		IDM_PRINT_ERROR(CvNetworkConnectionError, "Error logging in.");
		return false;
	}
	cout << " \t\t\tsuccess" << endl;

	// open the channel. name it "m_channel_id". test it.
	cout << "[RabbitMQ]: 	Opening channel... ";
	amqp_channel_open(m_conn_state, m_channel_id);
	if (!return_on_amqp_error(amqp_get_rpc_reply(m_conn_state), "Opening channel"))
	{
		IDM_PRINT_ERROR(CvNetworkConnectionError, "Error opening channel.");
		return false;
	}
	cout << " \t\tsuccess" << endl;

	// set the prefetch count to 1
	// prefetch count is the number of unacked requests rabbitmq can pull of the queue.
	cout << "[RabbitMQ]: 	Setting prefetch count to " << prefetchCount;
	amqp_basic_qos(m_conn_state, m_channel_id, 0, prefetchCount, 0);
	if (!return_on_amqp_error(amqp_get_rpc_reply(m_conn_state), "Setting prefetch count"))
	{
		IDM_PRINT_ERROR(CvNetworkConnectionError, "Error setting prefetch count.");
		return false;
	}
	cout << " \tsuccess" << endl;

	// start the consumption. test it
	cout << "[RabbitMQ]: 	Starting consumption...";
	amqp_basic_consume(m_conn_state, m_channel_id, m_render_queue_name, m_consumer_tag, 0, 0, 0, amqp_empty_table);
	if (!return_on_amqp_error(amqp_get_rpc_reply(m_conn_state), "Consuming"))
	{
		IDM_PRINT_ERROR(CvNetworkConnectionError, "Error starting RabbitMQ consumption.");
		return false;
	}
	cout << "\t\tsuccess" << endl;

	cout << "[RabbitMQ]: 	RabbitMQ listener initialized" << endl;
	m_init = true;
	return true;
}

//---------------------------------------------------------------------------------------------
void RabbitListener::CleanUp()
{
	m_init = false;
	if (0 != m_render_queue_name.len)
	{
		amqp_bytes_free(m_render_queue_name);
		amqp_bytes_zero(&m_render_queue_name);
	}
	if (0 != m_done_queue_name.len)
	{
		amqp_bytes_free(m_done_queue_name);
		amqp_bytes_zero(&m_done_queue_name);
	}
	if (0 != m_done_queue_default_name.len)
	{
		amqp_bytes_free(m_done_queue_default_name);
		amqp_bytes_zero(&m_done_queue_default_name);
	}
	m_renderer = nullptr;
}

//---------------------------------------------------------------------------------------------
bool RabbitListener::Start()
{
	// don't start it again if it's already started
	if (!m_done)
	{
		return false;
	}

	// it hasn't been started yet - go for it
	m_done = false;
	m_listener_thread = std::thread(&RabbitListener::Worker, this);
	IDMConcurrency::SetThreadName(m_listener_thread.native_handle(), "RabbitListener");

	return true;
}

//---------------------------------------------------------------------------------------------
bool RabbitListener::Stop()
{
	// if it hasn't been started yet, don't bother stopping
	if (m_done)
	{
		return false;
	}

	m_done = true;
	m_listener_thread.join();
	return true;
}

//---------------------------------------------------------------------------------------------
bool RabbitListener::IsInitialized() const
{
	return m_init;
}

//---------------------------------------------------------------------------------------------
void RabbitListener::WaitUntilDone()
{
	std::unique_lock<std::mutex> lk(m_doneMutex);
	m_doneCond.wait(lk, [this] { return (bool)m_quitRequested; });
	return;
}

//---------------------------------------------------------------------------------------------
Command* RabbitListener::ProcessCommand(const std::string& strCmd)
{
	// trim
	char ws[] = " \t\n\r";
	size_t strBegin = strCmd.find_first_not_of(ws);
	size_t strEnd = strCmd.find_last_not_of(ws);
	std::string strCmdTrim = strCmd.substr(strBegin, (strEnd - strBegin) + 1);
	
	if (!strCmdTrim.size()){
		return nullptr;
	}

	// tokenize
	std::vector<std::string> tokens;
	StringUtils::split(strCmdTrim, ' ', tokens);
	if (!tokens.size()){
		return nullptr;
	}


	// pull off first
	std::string firstToken = tokens[0];
	uint32_t iCmd;
	for (iCmd = (uint32_t)RenderCmd::eRCUnknown; iCmd < (uint32_t)RenderCmd::eRCnumberof; ++iCmd){
		if (0 == StringUtils::StrCmpI(firstToken.c_str(), RenderCmdStrMap[iCmd].str)){
			break;
		}
	}

	RenderCmd eCmd = (RenderCmd)iCmd;
	Command* cmdRet = nullptr;
	switch (eCmd){
	case eRCSetAsset:
	{
		if (tokens.size() < 3) return nullptr;
		CmdSetAsset cmd(m_renderer, 0, "");
		
		//cmd.m_id;
		char* strEnd;
		long converted = strtol(tokens[1].c_str(), &strEnd, 10);
		if (*strEnd) return nullptr;
		cmd.m_id = converted;

		//cmd.m_assetInfo;
		for (int i = 2; i < tokens.size(); ++i)
			cmd.m_assetInfo.append(tokens[i]);

		cmdRet = new CmdSetAsset(cmd);
		break;
	}
		
	case eRCSetFps:
	{
		if (tokens.size() < 2) return nullptr;
		CmdSetFps cmd(m_renderer, 0.0f);

		// cmd.m_fps
		char* strEnd;
		float converted = strtof(tokens[1].c_str(), &strEnd);
		if (*strEnd) return nullptr;
		cmd.m_fps = converted;

		cmdRet = new CmdSetFps(cmd);
		break;
	}
		
	case eRCSetState:
	{
		if (tokens.size() < 2) return nullptr;
		CmdSetState cmd(m_renderer, PlaybackState::ePSUnknown);
		std::string arg1 = tokens[1];
		// cmd.m_state
		// see if it's a number
		char* strEnd;
		long converted = strtol(arg1.c_str(), &strEnd, 10);
		if (0 == *strEnd) {
			// it's a number. test the range
			if ((converted <= PlaybackState::ePSUnknown) || (converted >= PlaybackState::ePSnumberof)) {
				return nullptr;
			}
			cmd.m_state = (PlaybackState)converted;
		}
		else {
			// it's a string
			uint32_t iState;
			for (iState = (uint32_t)PlaybackState::ePSUnknown; iState < (uint32_t)PlaybackState::ePSnumberof; ++iState){
				if (0 == StringUtils::StrCmpI(arg1.c_str(), PlaybackStateStrMap[iState].str)){
					break;
				}
			}
			if (iState >= (uint32_t)PlaybackState::ePSnumberof){
				return nullptr;
			}
			cmd.m_state = (PlaybackState)iState;
		}
		
		
		cmdRet = new CmdSetState(cmd);
		break;
	}
		
	case eRCSetFrameId:
	{
		if (tokens.size() < 2) return nullptr;
		CmdSetFrameId cmd(m_renderer, 0);

		// cmd.m_fps
		char* strEnd;
		long converted = strtol(tokens[1].c_str(), &strEnd, 10);
		if (*strEnd) return nullptr;

		cmd.m_frameId = converted;
		cmdRet = new CmdSetFrameId(cmd);
		break;
	}
		
	case eRCLoadIdm:
	{
		if (tokens.size() < 2) return nullptr;
		CmdLoadIdm cmd(m_renderer, "");
		
		//cmd.m_idmPath
		for (int i = 1; i < tokens.size(); ++i)
			cmd.m_idmPath.append(tokens[i]);

		cmdRet = new CmdLoadIdm(cmd);
		break;
	}
	default:
		return nullptr;
	}

	return cmdRet;
}

//---------------------------------------------------------------------------------------------
bool RabbitListener::IsShutdownCommand(const std::string& strCmd)
{
	// trim
	char ws[] = " \t\n\r";
	size_t strBegin = strCmd.find_first_not_of(ws);
	size_t strEnd = strCmd.find_last_not_of(ws);
	std::string strCmdTrim = strCmd.substr(strBegin, (strEnd - strBegin) + 1);

	if (!strCmdTrim.size()){
		return nullptr;
	}

	// tokenize
	std::vector<std::string> tokens;
	StringUtils::split(strCmdTrim, ' ', tokens);
	if (!tokens.size()){
		return nullptr;
	}

	if ((tokens.size() == 1) && 
		((0 == StringUtils::StrCmpI(tokens[0].c_str(), "shutdown")) ||
		(0 == StringUtils::StrCmpI(tokens[0].c_str(), "quit")) ||
		(0 == StringUtils::StrCmpI(tokens[0].c_str(), "exit")))
		){
		return true;
	}

	return false;

}

//---------------------------------------------------------------------------------------------
void RabbitListener::Finish(uint64_t id, uint32_t errorCode)
{
	lock_guard<mutex> lock(m_mutex);
	cout << "[RabbitMQ]:  ack()ing request " << id << endl;
	amqp_basic_ack(m_conn_state, m_channel_id, id, 0);
	cout << "[RabbitMQ]:   Finished with error code: " << errorCode << endl;
}

////---------------------------------------------------------------------------------------------
//void IDMRabbitListener::FinishJob(const JobReport& report)
//{
//	cout << "RabbitMQ: ack()ing request " << report.m_id << endl;
//	// this is the render_done function that:
//
//	//	(1) acks
//	{
//		lock_guard<mutex> lock(m_mutex);
//		amqp_basic_ack(m_conn_state, m_channel_id, report.m_id, 0);
//	}
//
//
//	//	(2) talks with the done_queue:
//	double msPerFrame = ((report.m_framesRendered == 0) ? 0 : (report.m_renderTimeMs / report.m_framesRendered));
//	double fps = ((report.m_framesRendered == 0) ? 0 : (1000.0 / msPerFrame));
//	cout << "RabbitMQ: FinishingJob() " << endl;
//	cout << "  request_id:\t" << report.m_id << endl;
//	cout << "  render_time:\t" << report.m_renderTimeMs << endl;
//	cout << "  frame_count:\t" << report.m_framesRendered << endl;
//	cout << "  fps:\t\t" << fps << " (" << msPerFrame << " ms/frame)" << endl;
//	cout << "  error_code:\t" << report.m_errorCode << " (" << ImsErrorMappingTable[report.m_errorCode].Message << ")" << endl;
//	if (report.m_errorCode != ImsNoErrors)
//	{
//#ifdef _WIN32
//		HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
//		SetConsoleTextAttribute(hStdOut, FOREGROUND_RED);
//#endif
//		cout << "  err_scene_id:\t" << report.m_errorSceneId << endl;
//		cout << "  err_Message:\t" << report.m_message << endl;
//#ifdef _WIN32
//		SetConsoleTextAttribute(hStdOut, FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_GREEN);
//#endif
//	}
//
//	// build the properties object
//	amqp_basic_properties_t props;
//	props._flags = AMQP_BASIC_CONTENT_TYPE_FLAG | AMQP_BASIC_DELIVERY_MODE_FLAG | AMQP_BASIC_HEADERS_FLAG;
//	props.content_type = amqp_cstring_bytes("text/plain");
//	props.delivery_mode = 2;
//
//	// build the headers table entries
//	const unsigned int entryCount = 5;
//	amqp_table_entry_t entries[entryCount];
//
//	entries[0].key = amqp_cstring_bytes("request_id");
//	entries[0].value.kind = AMQP_FIELD_KIND_I64;	// AMQP_FIELD_KIND_U64 not supported by rabbit, only amqp-0.9.1 (?)
//	entries[0].value.value.i64 = (int64_t)report.m_id;			//	https://www.rabbitmq.com/amqp-0-9-1-errata.html
//
//	entries[1].key = amqp_cstring_bytes("render_time");
//	entries[1].value.kind = AMQP_FIELD_KIND_F64;
//	entries[1].value.value.f64 = report.m_renderTimeMs;
//
//	entries[2].key = amqp_cstring_bytes("frame_count");
//	entries[2].value.kind = AMQP_FIELD_KIND_I32;
//	entries[2].value.value.i32 = report.m_framesRendered;
//
//	entries[3].key = amqp_cstring_bytes("error_code");
//	entries[3].value.kind = AMQP_FIELD_KIND_I64;
//	entries[3].value.value.i64 = (int64_t)report.m_errorCode;
//
//	entries[4].key = amqp_cstring_bytes("scene_id_error_catch");
//	entries[4].value.kind = AMQP_FIELD_KIND_I64;
//	entries[4].value.value.i64 = (int64_t)report.m_errorSceneId;
//
//	// build the table into the properties object
//	props.headers.num_entries = entryCount;
//	props.headers.entries = entries;
//
//	// set up the publish
//	amqp_bytes_t exchange = amqp_cstring_bytes("");
//	amqp_bytes_t message_body = amqp_cstring_bytes(report.m_message.c_str());
//	amqp_bytes_t routing_key;
//	amqp_bytes_zero(&routing_key);
//	if (m_done_queue_name.len != 0) {
//		routing_key = m_done_queue_name;
//	}
//	else if (m_done_queue_default_name.len != 0) {
//		routing_key = m_done_queue_default_name;
//	}
//	else {
//		printf("done-queue parameter wasn't set.\n");
//		return;
//	}
//
//
//
//		{
//			// do the publish
//			lock_guard<mutex> lock(m_mutex);
//			int val = amqp_basic_publish(m_conn_state, 1, exchange, routing_key, 1, 0, &props, message_body);
//			amqp_rpc_reply_t val2 = amqp_get_rpc_reply(m_conn_state);
//		}
//}


//---------------------------------------------------------------------------------------------
void RabbitListener::Worker()
{

	while (!m_done)
	{

		// check the done queue to see if there's anything to ack
		DoneRequest nextDoneRequest;
		while (m_renderer->GetNextDoneRequest(nextDoneRequest)){
			this->Finish(nextDoneRequest.id, nextDoneRequest.errorCode);
		}

		// try to release some memory if possible
		amqp_maybe_release_buffers(m_conn_state);

		amqp_envelope_t envelope;
		//timeval timeout_value = { IDM_LISTENER_THREAD_CONSUME_TIMEOUT_SEC, 0 };
		
		timeval timeout_value = { 0, IDM_LISTENER_THREAD_CONSUME_TIMEOUT_US };
		amqp_rpc_reply_t res = amqp_consume_message(m_conn_state, &envelope, &timeout_value, 0);

		if (AMQP_RESPONSE_NORMAL != res.reply_type)
		{
			if (AMQP_RESPONSE_LIBRARY_EXCEPTION == res.reply_type &&
				AMQP_STATUS_TIMEOUT == res.library_error)
			{
				// normal timeout -- give up for a round to check if the main program wants to shut down, etc
				continue;
			}
			else
			{
				// all other abnormal responses
				printf("[RabbitMQ]: Reply type was not a normal response.\n");
				amqp_basic_ack(m_conn_state, m_channel_id, envelope.delivery_tag, 0);
				continue;
			}
		}

		printf("[RabbitMQ]: Delivery: [%u] | exchange: [%.*s] | routingkey: [%.*s]\n",
			(unsigned)envelope.delivery_tag,
			(int)envelope.exchange.len, (char *)envelope.exchange.bytes,
			(int)envelope.routing_key.len, (char *)envelope.routing_key.bytes);

		if (envelope.message.properties._flags & AMQP_BASIC_CONTENT_TYPE_FLAG) {
			printf("[RabbitMQ]: Content-type: [%.*s]\n",
				(int)envelope.message.properties.content_type.len,
				(char *)envelope.message.properties.content_type.bytes);
		}


		if (0 == envelope.message.properties.headers.num_entries)
		{
			//this->Finish(envelope.delivery_tag, ImsInvalidCommand);
		}

		// if body is empty
		if (0 >= envelope.message.body.bytes){
			amqp_basic_ack(m_conn_state, m_channel_id, envelope.delivery_tag, 0);
		}

		string str_bod = string(static_cast<char*>(envelope.message.body.bytes), envelope.message.body.len);
		std::vector<std::string> lines;
		StringUtils::split(str_bod, '\n', lines);
		for (int i = 0; i < lines.size(); ++i)
		{
			// check if it's a shutdown command
			const std::string& currLine = lines[i];
			if (IsShutdownCommand(currLine))
			{
				m_quitRequested = true;
				m_doneCond.notify_one();
			}
			else
			{
				Command* newCmd = ProcessCommand(currLine);
				if (nullptr != newCmd){
					m_renderer->SubmitCommand(newCmd);
				}
			}
		}
		
		amqp_basic_ack(m_conn_state, m_channel_id, envelope.delivery_tag, 0);
		amqp_destroy_envelope(&envelope);
	}

	if (!return_on_amqp_error(amqp_channel_close(m_conn_state, 1, AMQP_REPLY_SUCCESS), "Closing channel"))
	{
		return;
	}

	if (!return_on_amqp_error(amqp_connection_close(m_conn_state, AMQP_REPLY_SUCCESS), "Closing connection"))
	{
		return;
	}

	if (!return_on_error(amqp_destroy_connection(m_conn_state), "Ending connection"))
	{
		return;
	}

	cout << "[RabbitMQ]: RabbitMQ listener thread ending..." << endl;
}

bool idm::cv::return_on_amqp_error(amqp_rpc_reply_t x, char const *context)
{
	switch (x.reply_type) {
	case AMQP_RESPONSE_NORMAL:
		return true;

	case AMQP_RESPONSE_NONE:
		fprintf(stderr, "[RabbitMQ]: %s: missing RPC reply type!\n", context);
		break;

	case AMQP_RESPONSE_LIBRARY_EXCEPTION:
		fprintf(stderr, "[RabbitMQ]: %s: %s\n", context, amqp_error_string2(x.library_error));
		break;

	case AMQP_RESPONSE_SERVER_EXCEPTION:
		switch (x.reply.id) {
		case AMQP_CONNECTION_CLOSE_METHOD: {
			amqp_connection_close_t *m = (amqp_connection_close_t *)x.reply.decoded;
			fprintf(stderr, "[RabbitMQ]: %s: server connection error %uh, message: %.*s\n",
				context,
				m->reply_code,
				(int)m->reply_text.len, (char *)m->reply_text.bytes);
			break;
		}
		case AMQP_CHANNEL_CLOSE_METHOD: {
			amqp_channel_close_t *m = (amqp_channel_close_t *)x.reply.decoded;
			fprintf(stderr, "[RabbitMQ]: %s: server channel error %uh, message: %.*s\n",
				context,
				m->reply_code,
				(int)m->reply_text.len, (char *)m->reply_text.bytes);
			break;
		}
		default:
			fprintf(stderr, "[RabbitMQ]: %s: unknown server error, method id 0x%08X\n", context, x.reply.id);
			break;
		}
		break;
	}

	return false;
}

bool idm::cv::return_on_error(int x, char const *context)
{
	if (x < 0) {
		fprintf(stderr, "[RabbitMQ]: %s: %s\n", context, amqp_error_string2(x));
		return false;
	}
	return true;
}
